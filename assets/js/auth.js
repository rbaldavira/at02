$(document).ready(function () {
    firebase.initializeApp(config);
    getUserLogado();
});

var config = {
    apiKey: "AIzaSyCay3gE2Pa0tPrtnYOQBpvC7dgkbZAKW6I",
    authDomain: "login-64cb1.firebaseapp.com",
    databaseURL: "https://login-64cb1.firebaseio.com",
    projectId: "login-64cb1",
    storageBucket: "login-64cb1.appspot.com",
    messagingSenderId: "370703097765"
  };

function loginFacebook() {
    var provedor = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provedor).then(function (result) {
        console.log('ok...');
        window.startSessionStorage(result.user);
        window.getDadosUsuario();
    }).catch(function (error) {
        // Handle Errors here.
        console.error("Falha: " + error);
        alert("Falha no login. Credenciais já utilizadas.");
        location.reload(true);
  // ...
    });
}

function loginGoogleAcc() {
    var provedor = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provedor).then(function (result) {
        console.log('ok...');
        window.startSessionStorage(result.user);
        window.getDadosUsuario();
    }).catch(function (error) {
        console.error("Falha: " + error);
        alert("Falha no login. Credenciais já utilizadas.");
        location.reload(true);
    });
}

function loginGithub() {
    var provedor = new firebase.auth.GithubAuthProvider();
    firebase.auth().signInWithPopup(provedor).then(function(result) {
        console.log('ok...');
            window.startSessionStorage(result.user);
            window.getDadosUsuario();
      // ...
    }).catch(function(error) {
      console.error("Falha: " + error);
      alert("Falha no login. Credenciais já utilizadas.");
      location.reload(true);
    });
};

function getUserLogado() {
    if (sessionStorage.nome) {
        getDadosUsuario();
    } else {
        logOut();
    }
}

function startSessionStorage(user) {
    sessionStorage.nome = user.displayName;
    sessionStorage.email = user.email;
    sessionStorage.avatar = user.photoURL;
}

function getDadosUsuario() {
    $("#name").text(sessionStorage.nome);
    $("#email").text(sessionStorage.email);
    $("#avatar").attr("src", sessionStorage.avatar);
    $(".welcome").removeClass("show");
    $("#login").addClass("show");
}

function logOut() {
    firebase.auth().signOut();
    $(".welcome").addClass("show");
    $("#login").removeClass("show");
    clearSessionStorage();
}

function clearSessionStorage() {
    sessionStorage.clear();
}