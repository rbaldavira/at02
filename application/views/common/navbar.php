<nav class="navbar navbar-toggleable-md " style="background-color: #FFCA28;">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand">
            <strong>Firebase</strong>
        </a>
        <div class="collapse navbar-collapse" id="navbarNav1">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url('index.php/Firebase/index') ?>" style="color: #000;">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('index.php/Firebase/api') ?>" style="color: #000;">API</a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('index.php/Firebase/login') ?>" class="nav-link" style="color: #000;">Authentication</a>
                </li>

            </ul>
        </div>
    </div>
</nav>