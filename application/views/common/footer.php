    <script type="text/javascript" src="<?= base_url('assets/js/jquery-3.1.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/tether.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/mdb.min.js') ?>"></script>
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <?= $script_list ?>
</body>

</html>