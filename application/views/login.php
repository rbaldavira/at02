<div class="container">
    <div class="text-center">

<div id="login" class="hide mt-5">
    <h2>Boas-vindas!</h2><br>
    <h5>Para continuar, escolha o método de login.</h5>
    <br>
    <div class="login">
        <button onclick="loginGoogleAcc()" class="btn btn-danger"><i class="fa fa-google" aria-hidden="true"></i>&nbsp;&nbsp;Google</button>
        <button onclick="loginFacebook()" class="btn btn-primary"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;<b>Facebook</b></button>
        <button onclick="loginGithub()" class="btn btn-info"><i class="fa fa-github" aria-hidden="true"></i>&nbsp;&nbsp;Github</button>
    </div>
</div>

<div class="welcome show mt-5">
    <div>
        <img class="circle" id="avatar" style="width: 150px; height: 150px;">
        <h4 class="mt-2" id="name"></h4>
        <h5 id="email"></h5>
    </div>
    <div style="margin-top: 50px">
        <button class="btn btn-unique" onclick="logOut()"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Sair</button>
    </div>
</div>
    </div>
</div>