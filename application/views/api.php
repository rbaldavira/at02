<div class="container">
    <div class="text-center">

<div class="hide mt-5">
    <h2>Sobre a API Firebase</h2><br>


    <div class="col-lg-12" style="margin: 0 auto; width: 50%">
    
    <p>A API utilizada é a função Authentication da Firebase.</p>

    <p>Seu objetivo é possibilitar, para qualquer aplicação, em múltiplas plataformas, a realização de login utilizando contas de provedores de identidades federadas. Isso aumenta o número de usuários que utilizam o sistema, pois evita
    o processo de criação de contas que podem ser apenas utilizadas em uma aplicação e viabiliza o login com um clique.</p>

    <p>As formas de login oferecidas pela API e escolhidas para este trabalho são: via conta do Google, via Facebook e via Github. Para as três, a API retorna o nome cadastrado no perfil, o e-mail utilizado e a imagem de perfil.</p>
                
    </div>

    
</div>