<?php

class Firebase extends MY_Controller {
    
    function __construct() {
        parent::__construct();
    }
            
    function index() {
        $html = $this->load->view('index', null, true);
        $this->show($html);
    }
    
    function api() {
        $html = $this->load->view('api', null, true);
        $this->show($html);
    }
    
    function login() {
        $this->addScript('auth');
        $html = $this->load->view('login', null, true);
        $this->show($html);
    }
    
}